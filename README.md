# Gitlab CI Extensions

`version: v1`

## На этой странице

- [Введение](#введение)
- [📒 Расширения](#-расширения)
  - [🛠 Build](#-build)
    - [`.build.Kaniko`](#buildkaniko)
  - [🚦 Test](#-test)
    - [Static Code Analysis and Lint](#static-code-analysis-and-lint)
      - [`.test.SuperLinter`](#testsuperlinter)
      - [`.test.Hawkeye`](#testhawkeye)
      - [`.test.Sonar`](#testsonar)
    - [Dockerfiles](#dockerfiles)
      - [`.test.DslimLint`](#testdslimlint)
      - [`.test.Hadolint`](#testhadolint)
    - [Container Images](#container-images)
      - [`.test.Trivy`](#testtrivy)
      - [`.test.Dockle`](#testdockle)
    - [Kubernetes](#kubernetes)
      - [`.test.Helm`](#testhelm)
      - [`.test.K8S`](#testk8s)
  - [📬 Deploy](#-deploy)
    - [`.deploy.Skopeo`](#deployskopeo)
    - [`.deploy.Helm.Push`](#deployhelmpush)
    - [`.deploy.Helm.Install`](#deployhelminstall)
    - [`.deploy.Kubectl.Apply`](#deploykubectlapply)
    - [`.deploy.Kubectl.Data`](#deploykubectldata)
- [🏗 Пайплайны](#пайплайны)
  - [`.pipeline.SCA`](#pipelinesca)
  - [`.pipeline.Image`](#pipelineimage)
- [📋 Образец файла `.gitlab-ci.yml` для проектов](#образецфайлаgitlab-ciymlдляпроектов)
- [Комментарии](#комментарии)

## Введение

#### Описание

Данный репозиторий описывает расширения для использования в GitLab CI/CD.

#### Использование

Для использования расширений необходимо в файле `.gitlab-ci.yml` целевого проекта подключить данный проект через [`include`](https://docs.gitlab.com/ce/ci/yaml/README.html#include-examples) и указать название на расширения через [`extends`](https://docs.gitlab.com/ce/ci/yaml/README.html#extends) в нужной задаче.

Опцию `include` необходимо указывать в самом верху файла `.gitlab-ci.yml`.

Пример подключение из любого сервера Gitlab:

```yml
include: "https://kukaryambik.gitlab.io/gitlab-ci-extensions/index.yml"
```

Из gitlab.com:

```yml
include:
  - project: "kukaryambik/gitlab-ci-extensions"
    file: "index.yml"
    ref: "v1"
```

Подключение расширений:

```yml
"My Little Job":
  extends: .job.Template
```

#### Переменные

[`variables`](https://docs.gitlab.com/ce/ci/yaml/README.html#variables)

Переменные, которые будут использованы при работе, можно указать как для всего проекта, так и для каждой задачи отдельно.

```yml
variables:
  globalVar: "true"

"My Little Job":
  extends: .job.Template
  variables:
    VARIABLE_1: "someValue"
    varName2: |
      value1
      Value2
```

При указании одной переменной несколько раз будет использована последняя. Также переменная для задачи имеет приоритет над глобальной.

Предопределенные переменные описаны в примечаниях к шаблонам и расширениям.

По умолчанию все переменные Gitlab имеют тип string, поэтому важно при указании переменной использовать кавычки.

## 📒 Расширения

[Расширения](/extensions) для включения в `.gitlab-ci.yml`.

---

### 🛠 Build

---

### `.build.Kaniko`

#### Описание

Сборка образов контейнеров с помощью иснструмента [Kaniko](https://github.com/GoogleContainerTools/kaniko).

#### Шаг по умолчанию

`stage: build`

#### Использование

```yml
"Build Image":
  extends: .build.Kaniko

"Build Second Image":
  extends: .build.Kaniko
  variables:
    BUILD_FILE: second.dockerfile
    BUILD_PATH: ./src/test
    BUILD_DST: |
      $CI_REGISTRY_IMAGE/some-image:$CI_COMMIT_SHORT_SHA
    BUILD_ARGS: |
      FOO=bar
      VAR=test
    BUILD_EXTRA_FLAGS: |
      --someFlag
      --andAnother
```

#### Переменные

| Переменная              | По умолчанию                | Описание                                                 |
| :---------------------- | --------------------------- | :------------------------------------------------------- |
| **`BUILD_DST`**         | `$CI_REGISTRY_IMAGE:latest` | Многострочная переменная. Определяет путь деплоя образа. |
| **`BUILD_PATH`**        | `.`                         | Путь от корня репозитория к директории сборки.           |
| **`BUILD_FILE`**        | `Dockerfile`                | Расположение Dockerfile относительно корня репозитория.  |
| **`BUILD_ARGS`**        |                             | Аргументы сборки, передаваемые с флагом `--build-arg`.   |
| **`BUILD_TARGET`**      |                             | Флаг `--target` для сборки определённого этапа.          |
| **`BUILD_EXTRA_FLAGS`** |                             | Дополнительные аргументы для команды сборки.             |

---

### 🚦 Test

---

### Static Code Analysis and Lint

---

### `.test.SuperLinter`

#### Описание

Проверка репозитория с помощью [GitHub Super-Linter](https://github.com/github/super-linter).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
SuperLinter:
  extends: .test.SuperLinter
```

#### Переменные

Список переменных для конфигурирования инструмента можно посмотреть на [официальной странице](https://github.com/github/super-linter#environment-variables).

#### Примечания

Для гибкой настройки отдельных линтеров воспользуйтесь [инструкцией в официальном репозитории проекта](https://github.com/github/super-linter/blob/master/docs/disabling-linters.md).

---

### `.test.Hawkeye`

#### Описание

Проверка репозитория с помощью [Hawkeye scanner-cli](https://github.com/hawkeyesec/scanner-cli).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
Hawkeye:
  extends: .test.Hawkeye
```

#### Переменные

| Переменная            | По умолчанию      | Описание                                                                                            |
| --------------------- | ----------------- | --------------------------------------------------------------------------------------------------- |
| **`HAWKEYE_ARGS`**    |                   | Дополнительные аргументы.                                                                           |
| **`HAWKEYE_EXCLUDE`** |                   | Исключенить из сканирования. Можно задать несколько значений через пробел.                          |
| **`HAWKEYE_FAILON`**  | `critical`        | Уровень критичности проблемы, начиная с которого  Hawkeye будет завершать работу с ненулевым кодом. |
| **`HAWKEYE_TARGET`**  | `$CI_PROJECT_DIR` | Директория проекта для сканирования.                                                                |

#### Примечания

Для гибкой настройки воспользуйтесь [инструкцией в официальном репозитории проекта](https://github.com/hawkeyesec/scanner-cli#configuration-files-recommended).

---

### `.test.Sonar`

#### Описание

Проверка кода статическим анализатором [SonarQube](https://www.sonarqube.org/).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
SonarQube:
  extends: .test.Sonar
```

#### Переменные

| Переменная                   | По умолчанию | Описание                                                           |
| ---------------------------- | ------------ | ------------------------------------------------------------------ |
| **`SONAR_HOST_URL`**         |              | URL сервера SonarQube.                                             |
| **`SONAR_TOKEN`**            |              | Токен доступа к SonarQube.                                         |
| **`SONAR_ARGS`**             |              | Дополнительные аргументы для `sonarscanner-cli`                    |
| **`SONAR_PROJECT_BASE_DIR`** | `.`          | Путь к основной директории проекта относительно корня репозитория. |

---

### Dockerfiles

---

### `.test.DslimLint`

#### Описание

Проверка Dockerfile с помощью [Docker Slim](https://dockersl.im/).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
Dslim:
  extends: .test.DslimLint
```

#### Переменные

| Переменная       | По умолчанию                 | Описание                                                |
| ---------------- | ---------------------------- | ------------------------------------------------------- |
| **`BUILD_FILE`** | `$CI_PROJECT_DIR/Dockerfile` | Расположение Dockerfile относительно корня репозитория. |

---

### `.test.Hadolint`

#### Описание

Проверка Dockerfile с помощью [Hadolint](https://github.com/hadolint/hadolint).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
Hadolint:
  extends: .test.Hadolint
```

#### Переменные

| Переменная       | По умолчанию                 | Описание                                                |
| ---------------- | ---------------------------- | ------------------------------------------------------- |
| **`BUILD_FILE`** | `$CI_PROJECT_DIR/Dockerfile` | Расположение Dockerfile относительно корня репозитория. |

---

### Container Images

---

### `.test.Dockle`

#### Описание

Проверка образов контейнеров с помощью [Dockle](https://github.com/goodwithtech/dockle).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
"Dockle":
  extends: .test.Dockle
```

#### Переменные

| Переменная         | По умолчанию         | Описание            |
| ------------------ | -------------------- | ------------------- |
| **`TARGET_IMAGE`** | `$CI_REGISTRY_IMAGE` | Образ для проверки. |

---

### `.test.Trivy`

#### Описание

Проверка образов контейнеров с помощью [Trivy](https://github.com/aquasecurity/trivy).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
"Test Image":
  extends: .test.Trivy
```

#### Переменные

| Переменная         | По умолчанию         | Описание            |
| ------------------ | -------------------- | ------------------- |
| **`TARGET_IMAGE`** | `$CI_REGISTRY_IMAGE` | Образ для проверки. |

#### Примечания

Также у Trivy есть свои переменные, которые можно посмотреть на [странице проекта](https://github.com/aquasecurity/trivy).

---

### Kubernetes

---

### `.test.Helm`

#### Описание

Проверка Helm чартов с помощью Helm Lint и [Kubeval](https://kubeval.instrumenta.dev/).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
Check:
  extends: .test.Helm
```

#### Переменные

| Переменная          | По умолчанию              | Описание                                                                  |
| ------------------- | ------------------------- | ------------------------------------------------------------------------- |
| **`K8S_VER`**       |                           | Версия Kubernetes на которой предполагается использование чарта.          |
| **`HELM_RELEASE`**  | `$CI_PROJECT_NAME`        | Название проекта (ReleaseName).                                           |
| **`HELM_CHART`**    |                           | Путь к Helm чарту для установки.                                          |
| **`HELM_VALUES`**   | `$HELM_CHART/values.yaml` | Путь к основному файлу values.                                            |
| **`HELM_VALUES_*`** |                           | Дополнительныке файлы values.                                             |
| **`HELM_VARS`**     |                           | Дополнительные значения values. Аналогично указанию флага `--set-string`. |

---

### `.test.K8S`

#### Описание

Проверка K8S манифестов с помощью [Kubeval](https://kubeval.instrumenta.dev/).

#### Шаг по умолчанию

`stage: test`

#### Использование

```yml
Check:
  extends: .test.K8S
```

#### Переменные

| Переменная         | По умолчанию | Описание                                                                                                          |
| ------------------ | ------------ | ----------------------------------------------------------------------------------------------------------------- |
| **`K8S_MANIFEST`** |              | Файлы манифестов для Kubernetes относительно корня репозитория. Если не задан, ищет файл `k8s.yml` в репозитории. |
| **`K8S_VER`**      |              | Версия целевого K8S кластера.                                                                                     |

---

### 📬 Deploy

#### Примечания

- В шаблонах деплоя в K8S имя пространства (namespace) соответствует названию окружения в Gitlab CI.
- Токен авторизации в Kubernetes должен быть задан для каждого типа окружения в настройках CI/CD проекта через переменную **`K8S_TOKEN`**.

---

### `.deploy.Skopeo.Copy`

#### Описание

Копирование образов контейнеров с помощью [Skopeo](https://github.com/containers/skopeo).

#### Шаг по умолчанию

`stage: deploy`

#### Использование

```yml
"Skopeo Copy":
  extends: .deploy.Skopeo.Copy
  variables:
    IMAGE_SRC: $CI_REGISTRY_IMAGE/old:latest
    IMAGE_DST: |
      $CI_REGISTRY_IMAGE/new:v1
```

---

### `.deploy.Helm.Push`

#### Описание

Деплой чарта в Helm репозиторий.

#### Шаг по умолчанию

`stage: deploy`

#### Использование

```yml
"Deploy Chart":
  extends: .deploy.Helm.Push

"Advanced Deploy Chart":
  extends: .deploy.Helm.Push
  variables:
    HELM_CHART: ./secondChart
    HELM_CHART_VERSION: $CI_COMMIT_SHORT_SHA
```

#### Переменные

| Переменная               | По умолчанию | Описание                                    |
| ------------------------ | ------------ | ------------------------------------------- |
| **`HELM_CHART`**         |              | Путь к Helm чарту для установки.            |
| **`HELM_CHART_VER`**     |              | Перезаписать версию чарта.                  |
| **`HELM_REPO`**          |              | Целевой репозиторий.                        |
| **`HELM_REPO_USERNAME`** |              | Имя пользователя для доступа к репозиторию. |
| **`HELM_REPO_PASSWORD`** |              | Пароль для доступа к репозиторию.           |

---

### `.deploy.Helm.Install`

#### Описание

Установка или обновление проекта с помощью Helm 3.

#### Шаг по умолчанию

`stage: deploy`

#### Использование

```yml
Deploy:
  extends: .deploy.Helm.Install
  environment: my-namespace

"Advanced Deploy":
  extends: .deploy.Helm.Install
  environment:
    name: my-namespace-dev
    url: https://my-project.0.example.com
  variables:
    HELM_CHART: ./helm
    HELM_VALUES: $HELM_CHART/values.yml
    HELM_VALUES_EXTRA: $SECRET_VALUES
    HELM_VARS: |
      app.password=$PASSWORD
    HELM_ARGS: >
      --set app.foo=bar
      --description 'TheBestExample'
```

#### Переменные

| Переменная           | По умолчанию              | Описание                                                                  |
| -------------------- | ------------------------- | ------------------------------------------------------------------------- |
| **`HELM_RELEASE`**   | `$CI_PROJECT_NAME`        | Название проекта (ReleaseName).                                           |
| **`HELM_CHART`**     |                           | Путь к Helm чарту для установки.                                          |
| **`HELM_CHART_VER`** |                           | Версия Helm чарта для установки.                                          |
| **`HELM_VALUES`**    | `$HELM_CHART/values.yaml` | Путь к основному файлу values.                                            |
| **`HELM_VALUES_*`**  |                           | Дополнительныке файлы values.                                             |
| **`HELM_VARS`**      |                           | Дополнительные значения values. Аналогично указанию флага `--set-string`. |
| **`HELM_ARGS`**      |                           | Дополнительные аргументы для Helm.                                        |

---

### `.deploy.Kubectl.Apply`

#### Описание

Применение манифестов через Kubectl.

#### Шаг по умолчанию

`stage: deploy`

#### Использование

```yml
Deploy:
  extends: .deploy.Kubectl.Apply
  environment: my-namespace
```

#### Переменные

| Переменная         | По умолчанию | Описание                                                                                                          |
| ------------------ | ------------ | ----------------------------------------------------------------------------------------------------------------- |
| **`K8S_MANIFEST`** |              | Файлы манифестов для Kubernetes относительно корня репозитория. Если не задан, ищет файл `k8s.yml` в репозитории. |

---

### `.deploy.Kubectl.Data`

#### Описание

Копирование данных в под.

#### Шаг по умолчанию

`stage: deploy`

#### Использование

```yml
"Deploy Data":
  extends: .deploy.Kubectl.Data
  environment: my-namespace
```

#### Переменные

| Переменная                | По умолчанию      | Описание                                                                                                                                               |
| ------------------------- | ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **`K8S_SELECTOR`**        |                   | Лейблы для выбора целевого пода.                                                                                                                       |
| **`K8S_CONTAINER`**       |                   | Целевой контейнер пода.                                                                                                                                |
| **`SOURCE_PATH`**         | `$CI_PROJECT_DIR` | Путь к источнику данных в репозитории или артефактам, которые необходимо скопировать.                                                                  |
| **`TARGET_PATH`**         |                   | Путь внутри контейнера, куда необходимо скопировать данные.                                                                                            |
| **`TARGET_CHOWN`**        |                   | Изменить владельца целевой директории.                                                                                                                 |
| **`EXCLUDE_PATH`**        |                   |                                                                                                                                                        | Исключить из копирования данные файлы и директории. |
| **`CLEANUP`**             | `"false"`         | Если `"true"`, очищает целевой каталог перед копированием.                                                                                             |
| **`CLEANUP_IGNORE_PATH`** |                   | Пути или wildcard для путей, которые должны остаться после очистки. Перечисляются через пробел. Например: `*/file.txt` или `*/uploads */example/*.log` |

### 🏗 Пайплайны

### `.pipeline.SCA`

#### Описание

Пайплайн проверки кода различными анализаторами.

Состоит из шаблонов раздела [Static Code Analysis and Lint](#static-code-analysis-and-lint).

### `.pipeline.Image`

#### Описание

Пайплайн сборки образа контейнера.

#### Состав

- `.pre`
  - [`.test.DslimLint`](#testdslimlint)
  - [`.test.Hadolint`](#testhadolint)
- `build`
  - [`.build.Kaniko`](#buildkaniko)
- `test`
  - [`.test.Trivy`](#testtrivy)
  - [`.test.Dockle`](#testdockle)
- `deploy`
  - [`.deploy.Skopeo.Copy`](#deployskopeocopy)

#### Шаг по умолчанию

`stage: build`

#### Использование

```yml
"Build Image":
  extends: .pipeline.Image
```

#### Примечания
- Этапы `.pre` и `test` критичны для Protected бранчей и тегов.
- Шаг `.deploy.Skopeo.Copy` необходим для добавления корректного тега к образу, так как на этапе сборки образ публикуется с временным тегом и сканируется по хешсумме, чтобы исключить подмену.
- Через родительскую задачу также можно передать переменные для конфигурации дочерних шагов.

## 📋 Образец файла `.gitlab-ci.yml` для проектов

```yml
# Общий набор шаблонов для использования
include: "https://kukaryambik.gitlab.io/gitlab-ci-extensions/index.yml"

# Сборка образа
"Build Image":
  extends: .pipeline.Image

# Деплой в окружение QA
"Deploy QA":
  extends: .deploy.Helm.Install
  variables:
    K8S_SERVER: https://example.com:6443/
    HELM_VALUES: ./helm/values.yml
    HELM_VALUES_SECRET: $HIDDEN_GITLAB_VARIABLES_FILE
  environment:
    name: example-qa

# Деплой в Prod
"Deploy PROD":
  extends: .deploy.Helm.Install
  variables:
    K8S_SERVER: https://example.com:6443/
    HELM_VALUES: ./helm/values.yml
    HELM_VALUES_PROD: ./extraValues.yaml
    HELM_VALUES_SECRET: $HIDDEN_GITLAB_VARIABLES_FILE
  environment:
    name: example-prod
```

## Комментарии

[**Идеи и предложения можно направлять через issue.**](/issues)
